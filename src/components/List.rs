use crate::components::project::Project;
use yew::prelude::*;

// The list can ONLY contain projects. No real logic behind this,
// Just wanted to see how the ChildrenWithProps type worked.
#[derive(Properties, PartialEq)]
pub struct ListProps {
    #[prop_or_default]
    pub children: ChildrenWithProps<Project>,
}

pub struct List {}

impl Component for List {
    type Message = ();
    type Properties = ListProps;

    fn create(_ctx: &Context<Self>) -> Self {
        Self {}
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
            <div class="list">
                {for ctx.props().children.iter()}
            </div>
        }
    }
}
