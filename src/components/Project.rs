use yew::prelude::*;

#[derive(Default)]
pub struct Project {}

// The information about a project will be passed as props
#[derive(Properties, PartialEq)]
pub struct ListProps {
    pub img: String,
    pub link: String,
    pub title: String,
    pub description: String,
}

impl Component for Project {
    type Message = ();
    type Properties = ListProps;

    fn create(_ctx: &Context<Self>) -> Self {
        Self {}
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        html! {
            <div class="card">
                <a href={ ctx.props().link.clone() }>
                    <img class ="project-image" src={ format!("./assets/{}", ctx.props().img.clone()) }/>
                </a>
                <p>{ctx.props().title.clone()}</p>
                <p>{ctx.props().description.clone()}</p>
            </div>
        }
    }
}
